// primostatemachine.h
//
// PrimoStateMachine class declaration
//
// Copyright (c) 2020 Project Primo. All rights reserved.

#ifndef __PRIMOSTATEMACHINE_PROJECT_PRIMO
#define __PRIMOSTATEMACHINE_PROJECT_PRIMO
#include <cassert>
#include <deque>
#include <random>
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include "sonararray.h"
#include "primostate.h"

class PrimoStateMachine {
public:
  PrimoStateMachine();
  PrimoState primoState() { return ps_; }
  PrimoState::State state() { return ps_.state(); }
  PrimoState::MetaState metastate() { return ps_.metastate(); }
  void set_metastate(PrimoState::MetaState ms) { ps_.set_metastate(ms); }
  // Updates the PrimoStateMachine based on sonar data in sa.  Outputs an
  // updated commanded velocity.
  void tick(const SonarArray& sa, geometry_msgs::Twist& cv);

private:
  PrimoState ps_;
  double turn_rate_;
  std::mt19937 mt_;
  std::uniform_real_distribution<double> dist_;
};

#endif
