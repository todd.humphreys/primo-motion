// main.cpp
//
// main function for primo-motion
//
// Copyright (c) 2020 Project Primo. All rights reserved.

#include <iostream>
#include <cassert>
#include <boost/program_options.hpp>
#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <opencv2/core/types.hpp>

#include "primostatemachine.h"
#include "sonararray.h"
#include "realsense.h"

namespace po = boost::program_options;

bool engaged = false;

void joy_callback(const sensor_msgs::Joy::ConstPtr& msg) {
  if(msg->buttons[0]) {
    engaged = true;
  }
  else if(msg->buttons[1]) {
    engaged = false;
  }
}

int main(int argc, char** argv) {

  // Example command-line arguments via Boost
  double linearSpeed = 0, angularSpeed = 0;
  po::options_description desc("Allowed options");
  desc.add_options()
    ("help,h", "produce help message")
    ("linear_speed,l", po::value<double>(&linearSpeed)->default_value(0.0), 
     "linear speed of robot (m/s)")
    ("angular_speed,a", po::value<double>(&angularSpeed)->default_value(0.0), 
     "angular speed of robot (rad/s)")
    ;
  po::variables_map vm;
  po::store(po::parse_command_line(argc,argv,desc),vm);
  po::notify(vm);
  if(vm.count("help")) {
    std::cout << desc << "\n";
    exit(EXIT_FAILURE);
  }

  // Set up ROS nodes
  ros::init(argc,argv,"primo_motion");
  ros::NodeHandle nh;
  SonarArray sa;
  ros::Publisher cv_pub = nh.advertise<geometry_msgs::Twist>("cmd_vel",1000);
  ros::Subscriber sonar_sub = nh.subscribe("sonars", 10, &SonarArray::callback, &sa);
  ros::Subscriber joy_sub = nh.subscribe("joy", 10, &joy_callback);
  constexpr double loop_rate_Hz = 10;
  ros::Rate loop_rate(loop_rate_Hz);
  
  // Declare and initialize commanded velocity
  geometry_msgs::Twist cv;
  cv.linear.x  = 0;
  cv.linear.y  = 0;
  cv.linear.z  = 0;
  cv.angular.x = 0;
  cv.angular.y = 0;
  cv.angular.z = 0;

  // Declare and initialize other variables
  int count = 0;
  PrimoStateMachine psm;
  Realsense realsense; 
  cv::Point target;
  
  // Our goal: Have the robot go as straight as it can, turning left or right
  // to avoid obstacles as necessary.
  bool sent_zeros = false;
  while(ros::ok()) {
    if(engaged){
      // Print out helpful info while engaged
      if(count % 10 == 0) {
        std::cout << "State: " << psm.state() << ",   __ " << std::setw(3) <<
          std::setprecision(3) << std::fixed << sa[4].range() <<
          ",     \\ " << sa[1].range() << ",    | " << sa[3].range() << ",    / " <<
          sa[2].range() << ",    __ " << sa[0].range() << std::endl;
      }
      realsense.grab_frames();
      bool found = realsense.find_target(target);
      // TODO:  modify the tick to take in a target or a delta to the target so that 
      // the robot moves in the direction of the target.
      
      psm.tick(sa, cv);
      
      cv_pub.publish(cv);
      sent_zeros = false;
    } 
    else {
      psm.set_metastate(PrimoState::NULLMETASTATE);
      cv.angular.z = 0;
      cv.linear.x = 0;
      if(!sent_zeros) {
        cv_pub.publish(cv);
        sent_zeros = true;
      }
    }

    ros::spinOnce();
    loop_rate.sleep();
    count++;
  }

  cv.linear.x = 0;
  cv.angular.z = 0;
  cv_pub.publish(cv);
  ros::spinOnce();
  return EXIT_SUCCESS;
}
