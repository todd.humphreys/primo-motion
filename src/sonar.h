// sonar.h
//
// Sonar class declaration 
//
// Copyright (c) 2020 Project Primo. All rights reserved.

#ifndef __SONAR_PROJECT_PRIMO
#define __SONAR_PROJECT_PRIMO
#include <cassert>

// The Sonar class is an abstraction of a sonar sensor.  It can be used to
// return the range measurement of a chosen sensor and bearing can be used for
// rotation detection of sensor.
class Sonar {
public:
  static constexpr double min_range = 0.05;
  static constexpr double max_range = 3.0;
  void init(int id) { range_ = max_range; bearing_ = 0; id_ = id; }
  // Returns true if range value is within valid range, otherwise returns
  // false
  bool valid() const { 
    if(min_range <= range_ && range_ <= max_range) {
      return true;
    }
    return false;	
  }
  // Returns the range measurement if valid, otherwise returns max_range
  double range() const { 
    if(valid()) {
      return range_;
    }
    else {
      return max_range;
    }
  }
  double bearing() const { return bearing_; }
  int id() const { return id_; }
  void set_range(double range) {
    range_ = range;
  }
  void set_bearing(double bearing) {
    bearing_ = bearing;
  }
  void set_id(int id) {
    id_ = id;
  }
  
private:
  // Sonar identifier number
  int id_;
  // Range to nearest object in meters
  double range_;
  // Bearing along which range measurement is taken, in radians measured
  // positive counterclockwise from the body X axis.
  double bearing_;
}; 

#endif
