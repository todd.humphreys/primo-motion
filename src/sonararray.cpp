// sonararray.cpp
//
// SonarArray class definition 
//
// Copyright (c) 2020 Project Primo. All rights reserved.

#include "sonararray.h"

SonarArray::SonarArray() {
  init();
}

void SonarArray::init() {
  array_.resize(nSensors);
  for(int ii=0; ii<nSensors; ii++) {
    array_[ii].init(ii);
  }
}

bool SonarArray::clear(double b1, double b2, double d) const {

  // Sort so that b1 is smaller 
  if(b1 > b2) {
    double temp = b2;
    b2 = b1;
    b1 = b2;
  }
  
  for(auto& ss : array_) {
    if(b1 <= ss.bearing() && ss.bearing() <= b2 && ss.range() <= d) {
      return false;
    }
  }
  return true;
}

Sonar& SonarArray::operator[](int ii) {
  assert(0 <= ii && ii < nSensors);
  for(auto& ss : array_) {
    if(ss.id() == ii) {
      return ss;
    }
  }
  throw std::runtime_error("No Sonar object found with the requested identifier.");
}

const Sonar& SonarArray::operator[](int ii) const {
  assert(0 <= ii && ii < nSensors);
  for(auto& ss : array_) {
    if(ss.id() == ii) {
      return ss;
    }
  }
  throw std::runtime_error("No Sonar object found with the requested identifier.");
}


// Compares two Sonar objects and returns true if the first object's range is
// greater than the second's.
bool is_greater(const Sonar& s1, const Sonar& s2) {
  return s1.range() > s2.range();
}

// Callback function that runs each time sonar data are available on the ROS
// topic /sonars
void SonarArray::callback(const sensor_msgs::Range::ConstPtr& msg) {
  std::string frame_id = msg->header.frame_id;
  if(frame_id == "sonar_0") {
    operator[](0).set_range(msg->range);
    operator[](0).set_bearing(-M_PI/2);
  }
  else if(frame_id == "sonar_1") {
    operator[](1).set_range(msg->range);
    operator[](1).set_bearing(M_PI/4);
  }
  else if(frame_id == "sonar_2") {
    operator[](2).set_range(msg->range);
    operator[](2).set_bearing(-M_PI/4);
  }
  else if(frame_id == "sonar_3") {
    operator[](3).set_range(msg->range);
    operator[](3).set_bearing(0);
  }
  else if(frame_id == "sonar_4") {
    operator[](4).set_range(msg->range);
    operator[](4).set_bearing(M_PI/2);
  }
  else {
    throw std::runtime_error("Unrecognized frame_id in /sonar topic");
  }
  // Sort Sonar objects in descending order
  std::sort (array_.begin(), array_.end(), is_greater);
}
