// primostate.h
//
// PrimoState class declaration
//
// Copyright (c) 2020 Project Primo. All rights reserved.

#ifndef __PRIMOSTATE_PROJECT_PRIMO
#define __PRIMOSTATE_PROJECT_PRIMO
#include <cassert>
#include <deque>
#include <ros/ros.h>

class PrimoState {

public:
  enum State {
    STOPPED = 0,
    TURNING_LEFT = 1,
    TURNING_RIGHT = 2,
    MOVING_STRAIGHT = 3,
    BACKING_LEFT = 4,
    BACKING_RIGHT = 5,
    BACKING_STRAIGHT = 6,
    SEARCHING_FOR_CLEAR_PATH = 7,
    NULLSTATE
  };

  enum MetaState {
    SEARCHING_FOR_TARGET = 0,
    TARGETING = 1,
    FIRING = 2,
    NULLMETASTATE
  };

  PrimoState();
  State state() { return state_; }
  MetaState metastate() { return metastate_; }
  std::deque<State> state_history() {return state_history_;}
  ros::Time state_time() { return state_time_; }
  void set_state(State s);
  void set_metastate(MetaState ms);
  void printHistory();

private:
  State state_; 
  MetaState metastate_;
  // Time at which state_ was first applied 
  ros::Time state_time_;
  // Records recent state history
  std::deque<State> state_history_;
  // Records the time at which each state in stateHistory_ was first applied 
  std::deque<ros::Time> time_history_;
};

#endif
