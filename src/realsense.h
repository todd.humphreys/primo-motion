// realsense.h
//
// Realsense class declaration 
//
// Copyright (c) 2020 Project Primo. All rights reserved.

#ifndef __REALSENSE_PROJECT_PRIMO
#define __REALSENSE_PROJECT_PRIMO
#include <librealsense2/rs.hpp>
#include <opencv2/core/types.hpp>

class Realsense {
public:
  Realsense();
  void grab_frames();
  bool find_target(cv::Point& target);
  cv::Point sight_center() const { return sight_center_; }

private:
rs2::pipeline pipe_;
rs2::frameset frames_;
cv::Point sight_center_; 
};

#endif
