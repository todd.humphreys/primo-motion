// sonararray.h
//
// SonarArray class declaration 
//
// Copyright (c) 2020 Project Primo. All rights reserved.

#ifndef __SONARARRAY_PROJECT_PRIMO
#define __SONARARRAY_PROJECT_PRIMO
#include <cassert>
#include <vector>
#include <sensor_msgs/Range.h>
#include "sonar.h"

// SonarArray contains an array of Sonar objects and member functions that
// operate on the array.
class SonarArray {
public:
  static constexpr int nSensors = 5;
  SonarArray();
  void init();
  void callback(const sensor_msgs::Range::ConstPtr& msg);
  // Returns a reference to the Sonar object with identifier ii
  Sonar& operator[] (int ii);
  // Returns a const reference to the Sonar object with identifier ii
  const Sonar& operator[](int ii) const;
  // Returns a const reference to the Sonar object with the iith longest
  // range, where ii = 0 corresponds to the longest range
  const Sonar& get_sorted(int ii) const { return array_[ii]; }
  // Returns a const reference to the Sonar object with the maximum range
  const Sonar& max() const { return array_[0]; }
  // Returns a const reference to the Sonar object with the minimum range
  const Sonar& min() const { return array_[nSensors - 1]; }
  // Returns true if all Sonar objects with bearing angle between b1 and b2
  // radians indicate a range greater than d meters.
  bool clear(double b1, double b2, double d) const; 

private:
  // Vector of Sonar objects, one for each sensor, sorted by decreasing range
  std::vector<Sonar> array_;
};

#endif
