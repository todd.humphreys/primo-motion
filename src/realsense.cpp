// realsense.cpp
//
// Realsense class definition 
//
// Copyright (c) 2020 Project Primo. All rights reserved.

#include <cassert>
#include <vector>
#include <iostream>
#include <opencv2/objdetect.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include "realsense.h"

Realsense::Realsense() : sight_center_(289,225) {
  
  rs2::config cfg;
  cfg.enable_stream(RS2_STREAM_COLOR, 640, 480, RS2_FORMAT_BGR8, 30);
  pipe_.start(cfg);
}

void Realsense::grab_frames() {
  frames_ = pipe_.wait_for_frames();
}

bool Realsense::find_target(cv::Point& target) {

  rs2::frame color_frame = frames_.get_color_frame();
  // Create OpenCV Matrix from a color image
  cv::Mat frame(cv::Size(640, 480), CV_8UC3, (void*) color_frame.get_data(),
                cv::Mat::AUTO_STEP);
  // Flip the image to account for upside-down camera
  cv::flip(frame,frame,-1);

  // Create a people detector 
  cv::HOGDescriptor hog;
  hog.setSVMDetector(cv::HOGDescriptor::getDefaultPeopleDetector());
  std::vector<cv::Rect> foundRectangles;
  std::vector<double> foundWeights;
  hog.detectMultiScale(frame, foundRectangles, foundWeights,
                       0, cv::Size(4,4), cv::Size(4,4), 1.05, 2.0, false);
  for (int ii = 0; ii < foundRectangles.size(); ii++) {
    const cv::Rect& rect = foundRectangles.at(ii);
    std::cout << "score: " << foundWeights.at(ii) << std::endl;
    cv::rectangle(frame, rect.tl(), rect.br(), cv::Scalar(0,255,0),2);
  }
  
  // Display image in a GUI
  cv::Rect mark(289,225,10,10);
  cv::rectangle(frame, mark.tl(), mark.br(), cv::Scalar(255,0,0),2);
  cv::namedWindow("Display Image", cv::WINDOW_AUTOSIZE );
  cv::imshow("Display Image", frame);
  cv::waitKey(0);

  // TODO: make the human detector better by using the Daimler detector in addition 
  // to the current one, and program aiming

  return false;
}

 


 
  

  