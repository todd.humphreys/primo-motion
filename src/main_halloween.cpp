// main.cpp
//
// main function for primo-motion: halloween version
//
// Copyright (c) 2020 Project Primo. All rights reserved.

#include <iostream>
#include <cassert>
#include <boost/program_options.hpp>
#include <ros/ros.h>
#include <sensor_msgs/Joy.h>

namespace po = boost::program_options;

bool hand_up = false;
bool hand_down = false;
bool hand_shake = false;

void joy_callback(const sensor_msgs::Joy::ConstPtr& msg) {
  if(msg->buttons[0]) {
    hand_up = true;
  }
  else if(msg->buttons[1]) {
    hand_down = true;
  }
  else if(msg->buttons[2]) {
    hand_shake = true;
  }
}

int main(int argc, char** argv) {

  // Example command-line arguments via Boost
  po::options_description desc("Allowed options");
  desc.add_options()
    ("help,h", "produce help message")
    ;
  po::variables_map vm;
  po::store(po::parse_command_line(argc,argv,desc),vm);
  po::notify(vm);
  if(vm.count("help")) {
    std::cout << desc << "\n";
    exit(EXIT_FAILURE);
  }

  // Set up ROS nodes
  ros::init(argc,argv,"primo_motion");
  ros::NodeHandle nh;
  ros::Subscriber joy_sub = nh.subscribe("joy", 10, &joy_callback);
  constexpr double loop_rate_Hz = 10;
  ros::Rate loop_rate(loop_rate_Hz);
  
  while(ros::ok()) {

    if(hand_up){
      std::string command = "ssh ubuntu@neon 'pigs s 18 1300'";
      if(system(command.c_str()) != 0) {
        std::cerr << "GPIO command to actuate trigger servo failed." << std::endl;
      }
      // Sleep to allow time for servo to move
      ros::Duration(1.0).sleep();
      hand_up = false;
    }
    if(hand_down){
      std::string command = "ssh ubuntu@neon 'pigs s 18 1080'";
      if(system(command.c_str()) != 0) {
        std::cerr << "GPIO command to actuate trigger servo failed." << std::endl;
      }
      // Sleep to allow time for servo to pull trigger
      ros::Duration(1.0).sleep();
      hand_down = false;
    }
    if(hand_shake){
      std::string command = "ssh ubuntu@neon 'pigs s 18 1300 mils 500 s 18 1200 mils 500 s 18 1300 mils 500 s 18 1200'";
      if(system(command.c_str()) != 0) {
        std::cerr << "GPIO command to actuate trigger servo failed." << std::endl;
      }
      // Sleep to allow time for servo to pull trigger
      ros::Duration(1.0).sleep();
      hand_shake = false;
    }

    ros::spinOnce();
    loop_rate.sleep();
  }

  ros::spinOnce();
  return EXIT_SUCCESS;
}
