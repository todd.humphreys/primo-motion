// primostatemachine.cpp
//
// PrimoStateMachine class definition
//
// Copyright (c) 2020 Project Primo. All rights reserved.

#define _USE_MATH_DEFINES
#include <cmath>
#include "primostatemachine.h"
#include "primostate.h"
#include <iostream>
#include <cstdlib>
#include <string>

std::random_device g_rd;
int stop_confirm =  0 ;
constexpr int stop_confirm_threshold  = 3;

// Stop_confirm used in tandom with "stop_confirmthreshhold" 
// to ask for another sonar cast to decrease false readings.

PrimoStateMachine::PrimoStateMachine() : mt_(g_rd()), dist_(-1,1) {
  ps_.set_state(PrimoState::MOVING_STRAIGHT);
  turn_rate_ = 0.1;
}

void PrimoStateMachine::tick(const SonarArray& sa, geometry_msgs::Twist& cv) {

  constexpr double deg2rad = M_PI/180;
  constexpr double move_duration = 1.5;
  constexpr double min_safe_range_ahead = 0.6;
  constexpr double min_safe_range_oblique = 0.3;
  constexpr double min_safe_range_side = 0.2;
  constexpr double nominal_velocity = 0.3;
  bool searching = false;
  int notMovingForwardCount = 0;

  // Calculate elapsed time in current state
  ros::Duration rdt = ros::Time::now() - ps_.state_time();
  double dt = rdt.toSec();
  for (int i = 1; i < ps_.state_history().size() && i < 12; i += 2){
    if (ps_.state_history().at(i) != PrimoState::MOVING_STRAIGHT){
      notMovingForwardCount++;
    }
  }
  if (notMovingForwardCount == 6){
    ps_.set_state(PrimoState::SEARCHING_FOR_CLEAR_PATH);
  }
  notMovingForwardCount = 0;

  // Implement metastate transition logic
  switch (ps_.metastate()) {
  case PrimoState::FIRING: {
    std::string command = "ssh ubuntu@neon 'pigs s 18 1630 mils 500 s 18 1400'";
    if(system(command.c_str()) != 0) {
      std::cerr << "GPIO command to actuate trigger servo failed." << std::endl;
    }
    // Sleep to allow time for servo to pull trigger
    ros::Duration(1.0).sleep();
    ps_.set_metastate(PrimoState::NULLMETASTATE);
    break; 
  }
  default:
    ps_.set_metastate(PrimoState::NULLMETASTATE);
  }

  // Implement state transition logic
  switch (ps_.state()){
  case PrimoState::TURNING_LEFT :
    if(dt > move_duration) {
      ps_.set_state(PrimoState::MOVING_STRAIGHT);
    }
    else {
      cv.angular.z = turn_rate_;
      cv.linear.x = 0;
    }
    break;  
  
  case PrimoState::TURNING_RIGHT : 
    if(dt > move_duration) {
      ps_.set_state(PrimoState::MOVING_STRAIGHT);
    }
    else {
      cv.angular.z = turn_rate_;
      cv.linear.x = 0;
    }
    break;

  case PrimoState:: BACKING_LEFT:
    if(dt > move_duration) {
      ps_.set_state(PrimoState::MOVING_STRAIGHT);
    }
    else {
      cv.angular.z = -30*deg2rad/move_duration;
      cv.linear.x = -0.5/move_duration;
    }
    break;

 case PrimoState:: BACKING_STRAIGHT:
    if(dt > move_duration) {
      ps_.set_state(PrimoState::STOPPED);
    }
    else {
      cv.angular.z = 0;
      cv.linear.x = -0.5/move_duration; 
    }
    break;

 case PrimoState::BACKING_RIGHT:
    if(dt > move_duration) {
      ps_.set_state(PrimoState::MOVING_STRAIGHT);
    }
    else {
      cv.angular.z = 30*deg2rad/move_duration;
      cv.linear.x = -0.5/move_duration;
    }
    break;


  case PrimoState::STOPPED :

    cv.linear.x = 0;
    cv.angular.z = 0;
    // Break out of STOPPED state if forward-facing sonars indicate all
    // clear
    if(sa.clear(-M_PI/4, M_PI/4, min_safe_range_ahead)) {
      ps_.set_state(PrimoState::MOVING_STRAIGHT);
    }   
    else if(sa.clear(-M_PI/2,-M_PI/2, 0.3)) {
      ps_.set_state(PrimoState::BACKING_RIGHT); 
    }   
    else if(sa.clear(M_PI/2,M_PI/2, 0.3)) {
      ps_.set_state(PrimoState::BACKING_LEFT); 
    }   
    else{
      ps_.set_state(PrimoState::BACKING_STRAIGHT);
    }
    break;

  case PrimoState::MOVING_STRAIGHT :
  
  
    if(sa.clear(-M_PI/4, M_PI/4, min_safe_range_ahead)) {
      stop_confirm = 0;
      cv.linear.x = nominal_velocity;
      cv.angular.z = 0;
    }
    else {
      stop_confirm +=1; 
      if(stop_confirm >= stop_confirm_threshold){
        if(sa[1].range() <= min_safe_range_oblique 
           && sa[2].range() <= min_safe_range_oblique){
          ps_.set_state(PrimoState::STOPPED);
        }
        else if(sa[1].range() <= sa[2].range()) {
          if(sa[4].range() < min_safe_range_side ||
             sa[0].range() < min_safe_range_side) {
            ps_.set_state(PrimoState::STOPPED);
          }
          else {
            ps_.set_state(PrimoState::TURNING_RIGHT);
            turn_rate_ = -(30 + 25*dist_(mt_))*deg2rad/move_duration;
          }
        } 
        else {
          if(sa[4].range() < min_safe_range_side ||
             sa[0].range() < min_safe_range_side) {
            ps_.set_state(PrimoState::STOPPED);
          }
          else {
            ps_.set_state(PrimoState::TURNING_LEFT);
            turn_rate_ = (30 + 25*dist_(mt_))*deg2rad/move_duration;
          }
        } 
      }
    }
    break;

  case PrimoState::SEARCHING_FOR_CLEAR_PATH :
    if (!searching){
    cv.linear.x = -.1;
    cv.angular.z = 0;
    }
    if(sa[4].range() >= min_safe_range_side && sa[0].range() >= min_safe_range_side){
      searching = true;
      cv.linear.x = 0;
      cv.angular.z = 30*deg2rad/move_duration;
      if (sa.clear(-M_PI/4, M_PI/4, min_safe_range_ahead)){
        searching = false;
        ps_.set_state(PrimoState::MOVING_STRAIGHT);
      }
    }
  }
}
