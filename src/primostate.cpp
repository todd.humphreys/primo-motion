// primostate.cpp
//
// PrimoState class definition
//
// Copyright (c) 2020 Project Primo. All rights reserved.

#include <iostream>
#include "primostate.h"


PrimoState::PrimoState() {
	state_ = NULLSTATE;
	metastate_ = NULLMETASTATE;
}

void PrimoState::printHistory() {

	std::cout << "state        time" << std::endl;
	for(int ii=0; ii < state_history_.size(); ii++) {
		std::cout << state_history_[ii] << "    " << 
		time_history_[ii] << std::endl;
	}
}

void PrimoState::set_state(State s) { 
	if(state_ != NULLSTATE) {
    	state_history_.push_front(state_);
    	time_history_.push_front(state_time_); 
	}
    state_ = s; 
    state_time_ = ros::Time::now();
}

void PrimoState::set_metastate(MetaState ms) { 
  metastate_ = ms; 
}





















